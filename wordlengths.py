#!/usr/bin/python3
# FIle name: word_lengths.py
# Author: Jasper Wieten

def main():
    countDict = {}
    linecount = 0
    data = open("Cleaned_Dataset.txt", "r")
    for line in data:
        linecount += 1
        countDict[str(len(line.strip()))]=countDict.get(str(len(line.strip())),0)+1
    print(countDict)
main()
